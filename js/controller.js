function renderContent(array) {
  let input = document.getElementById("newTask").value;
  array.push(input);
  let contentHTML = "";
  array.forEach((item) => {
    contentLi = `
    <li>
    <td>${item}</td>
    <td><div class="buttons">
    <button onclick="removeTask()" class="remove"><i class="fa fa-trash-alt"></i></button>
    <button class="complete"><i class="fa fa-check-circle"></i></button>
    </div></td>
    </li>
    `;
    contentHTML += contentLi;
  });
  document.getElementById("todo").innerHTML = contentHTML;
}
